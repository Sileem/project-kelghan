package Kelghan.Overworld;

import Kelghan.*;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferStrategy;

/**
 * This module should be in charge of drawing O/W sprites to the screen and
 * handling PC movement on the screen, as well as dealing with town/dungeon entities
 * and random battles if implemented.
 * 
 * @author Sileem
 */
public class Overworld {
    private Graphics g = null;
    private BufferStrategy strategy;
    
    public Overworld(Graphics g, BufferStrategy strategy) {
	this.g = g;
	this.strategy = strategy;
    }
    
    public void OverworldLoop() {
	while(Game.overWorldMod) {
	    g = (Graphics2D) strategy.getDrawGraphics();
	    g.setColor(Color.GREEN);
	    g.fillRect(0, 0, 800, 600);
	    
	    // This is where the magic happens
	    
	    g.dispose();
	    strategy.show();
	    try { Thread.sleep(17); } catch(Exception e) {}
	 }
    }
}
