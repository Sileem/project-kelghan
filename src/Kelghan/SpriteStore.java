package Kelghan;

import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import javax.imageio.ImageIO;

/**
 *
 * @author Sileem
 */
public class SpriteStore {
    /* There is a single instance of this class */
    private static SpriteStore single = new SpriteStore();
    /* Cached map of sprites */
    public HashMap Sprites = new HashMap();
    
    /**
     * Get the single instance of this class
     * 
     * @return The single instance of this class
     */
    public static SpriteStore get() {
	return single;
    }
    
    /**
     * retrieve a sprite from the hashmap
     * 
     * @param ref Index of the sprite
     * @return A sprite instance containing an accelerated image of the requested reference
     */
    public Sprite getSprite(String ref) {
	/* If the Sprite is cached, return the sprite */
	if(Sprites.get(ref) != null) {
	    return (Sprite) Sprites.get(ref);
	}
	
	// Otherwise get the sprite from resource loader
	// and store it in the hashmap
	
	BufferedImage SourceImage = null;
	
	try {
	    URL url = this.getClass().getClassLoader().getResource(ref);
	    
	    if (url == null) {
		// Failed to find
		fail("Failed to find" + ref);
	    }
	    
	    SourceImage = ImageIO.read(url);
	} catch(IOException e) {
	   // Failed to load 
	    fail("Failed to load" + ref);
	}
	// Create an accelerated image to store the sprite
	GraphicsConfiguration gc = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice()
				   .getDefaultConfiguration();
	Image image = gc.createCompatibleImage(SourceImage.getWidth(), SourceImage.getHeight(), Transparency.BITMASK);
	
	// Draw the source onto the accelerated image
	image.getGraphics().drawImage(SourceImage, 0, 0, null);
	
	// Finally, Create the sprite and add it to cache
	Sprite sprite = new Sprite(image);
	Sprites.put(ref,sprite);
	
	return sprite;
    }
    
    private void fail(String message) {
	// TODO: Don't freak out
	System.err.println(message);
	System.exit(0);
    }
}
