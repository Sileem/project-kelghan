/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Kelghan;

import java.awt.Graphics;
import java.awt.Image;

/**
 *
 * @author Sileem
 */
public class Sprite {
    private Image image;
    
    /**
     *  Create a sprite based off an image
     * 
     * @param image. The image that's in the sprite
     */
    public Sprite(Image img) {
	this.image = img;
    }
    /**
     * Get the width of the sprite
     * 
     * @return The Width of the sprite in pixels
     */
    public int getWidth(){
	return image.getWidth(null);
    }
    /**
     * Get the height of the sprite
     * 
     * @return The height of the sprite in pixels
     */
    public int getHeight() {
	return image.getHeight(null);
    }
    /**
     * Draw the sprite onto the context
     * @param g The graphics context on which to draw the sprite
     * @param x The x location
     * @param y The y location
     */
    public void draw(Graphics g, int x, int y) {
	g.drawImage(image, x, y, null);
    }
    
}
