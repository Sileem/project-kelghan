package Kelghan;

import Kelghan.Entity.*;
import Kelghan.Overworld.Overworld;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferStrategy;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author Sileem
 */
public class Game extends Canvas{
    /* Debug flag */
    public static boolean debug = true; 
    
    public static BufferStrategy strategy;
    private boolean gameRunning = true;
    
    private ArrayList Entities = new ArrayList();
    
    /* Accelerated graphics layer */
    public static Graphics2D g = null;
    
    /* Player is in Overworld */
    public static boolean overWorldMod = true;
    /* Player is in town */
    public static boolean townMod = false;
    /* Player is in Battle */
    public static boolean battleMod = false;
    /* Player is in menu */
    public static boolean menuMod = false;
    
    /* PC */
    public static Entity pc;
    
    /* Booleans of the directional buttons */
    public static boolean isLeftPressed = false;
    public static boolean isRightPressed = false;
    public static boolean isUpPressed = false;
    public static boolean isDownPressed = false;
    /* Event keys */
    public static boolean isInteractPressed = false;
    public static boolean isMenuPressed = false;
    public static boolean isCancelPressed = false;
    
    public Game() {
        JFrame container = new JFrame("Project Kelghan");
        
        JPanel panel = (JPanel) container.getContentPane();
        panel.setPreferredSize(new Dimension(800,600));
        panel.setLayout(null);
        
        setBounds(0,0,800,600);
        panel.add(this);
	
        
        setIgnoreRepaint(true);
        
        container.pack();
        container.setResizable(false);
        container.setVisible(true);
	
        // Close the game the old fashioned way
        container.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        
        // Create window buffer strategy
        createBufferStrategy(2);
        strategy = getBufferStrategy();
	
	container.requestFocus();

	addKeyListener(new Input());
    }
    public void gameLoop() {
	pc = new Player(this, "Sprites/pc.gif",400,300);
        while(gameRunning) {
	    // Get Graphics context for the accelerated graphics
	    g = (Graphics2D) strategy.getDrawGraphics();
	    g.setColor(Color.GREEN);
	    g.fillRect(0, 0, 800, 600);
	    
	    if(debug = true) {
		/* Print out the PC cooridnates */
		System.out.println("x = " + pc.getX() + " y = " + pc.getY());
	    }
	    
	    /* Move PC */
//	    if(isUpPressed) {
//		pc.move('U');
//	    }
//	    else if(isDownPressed) {
//		pc.move('D');
//	    }
//	    else if(isRightPressed) {
//		pc.move('R');
//	    }
//	    else if(isLeftPressed) {
//		pc.move('L');
//	    }
//	    pc.draw(g);
	    
	    Overworld ow = new Overworld(g, strategy);
	    ow.OverworldLoop();
	    
	    
		 
	    // Flip the buffer
	    g.dispose();
	    strategy.show();
	    
	    
	    try { Thread.sleep(17); } catch(Exception e) {} // Cheap way of getting 60fps
        }
    }
    

}
