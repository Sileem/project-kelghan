/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Kelghan.Entity;

import Kelghan.*;

/**
 * The entity which represents the player
 * 
 * @author Sileem
 */
public class Player extends Entity{
    private Game game;
    
    /**
     * Create the player entity
     * 
     * @param game The game to which it is being created in
     * @param ref Reference to the character sprite
     * @param x The initial x location
     * @param y The initial y location
     */
    public Player(Game game, String ref, int x, int y) {
	super(ref, x, y);
	
	this.game = game;
    }
    
    public void move(char dir) {
	super.move(dir);
    }

    @Override
    public void collidedWith(Entity other) {
    }
}
