package Kelghan.Entity;

import Kelghan.Sprite;
import Kelghan.SpriteStore;
import java.awt.Graphics;
import java.awt.Rectangle;

/**
 *
 * @author Sileem
 */
public abstract class Entity {
    /** The x and y coordinates of the entity */
    protected double x;
    protected double y;
    /** The sprite associated with the entity */
    protected Sprite sprite;
    /** The current speed of the entity (horizontal) (pixels/sec) */
    protected double dx;
    /** The current speed of the entity (Vertical) (pixels/sec) */
    protected double dy;
    /** The rectangle of this entity used for collision */
    private Rectangle me = new Rectangle();
    /** The rectangle of other entities used for collision */
    private Rectangle him = new Rectangle();
    
    /**
     * Construct the entity
     * @param ref The reference to the image to be displayed for this entity
     * @param x The initial x location
     * @param y The initial y location
     */
    public Entity(String ref, int x, int y) {
	this.sprite = SpriteStore.get().getSprite(ref);
	this.x = x;
	this.y = y;
    }
    
    /**
     * Request that the entity move it self
     * @param dir The direction movement
     */
    public void move(char dir){
	// Updates the x,y coordinates based on the direction of movement
	// TODO: Smooth transition
	/* Movement speed of 6.4pixels/tick which lines up with 32pixel tiles */
	
	if(dir == 'U') {
	    /* Entity moves up */
	    y -= 6.4;
	    return;
	}
	if(dir == 'D') {
	    /* Entity moves down */
	    y += 6.4;
	    return;
	}
	if(dir == 'R') {
	    /* Entity moves right */
	    x += 6.4;
	    return;
	}
	if(dir == 'L') {
	    /* Entity moves left */
	    x -= 6.4;
	    return;
	}
    }
    
    /**
     * Get the horizontal speed of the entity
     * @return The horizontal speed of the entity (pixels/sec)
     */
    public double getHorizontalSpeed() { return dx; }
    
    /**
     * Get the vertical speed of the entity
     * @return The vertical speed of the entity (Pixels/sec)
     */
    public double getVerticalSpeed() { return dy; }
    
    /**
     * Draw the entity to the graphics context provided
     * @param g The graphics context on which to draw the entity
     */
    public void draw(Graphics g) {
	sprite.draw(g, (int) x, (int) y);
    }
    
    /**
     * Get the x location of this entity
     * @return The x location of this entity
     */
    public int getX() { return (int) x; }
    
    /**
     * Get the y location of this entity
     * @return The y location of this entity
     */
    public int getY() { return (int) y; }
    
    /**
     * Check if there is collision between entities
     * @param other The entity to check with if there's collision
     * @return True if the entites collide. Otherwise False.
     */
    public boolean collidesWith(Entity other) {
	// TODO: Implement tile movement
	me.setBounds((int) x, (int) y, sprite.getWidth(), sprite.getHeight());
	him.setBounds((int) other.x, (int) other.y, other.sprite.getWidth() , other.sprite.getHeight());
	
	return me.intersects(him);
    }
    
    public void doLogic() {
	//TODO: Logic
    }
    
    /**
     * Notification that this entity collided with another
     * @param other The entity with which this one collided with
     */
    public abstract void collidedWith(Entity other);
    
    
}
