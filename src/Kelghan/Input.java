package Kelghan;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Keyboard listener class
 *
 * @author Sileem
 */
public class Input extends KeyAdapter{
    // TODO: Better control scheme
    // TODO: User Definable control scheme
    
    public void keyPressed(KeyEvent e) {
	/**
	 * Directional keys
	 */
	if(e.getKeyCode() == KeyEvent.VK_UP) {
	    Game.isUpPressed = true;
	}
	if(e.getKeyCode() == KeyEvent.VK_DOWN) {
	    Game.isDownPressed = true;
	}
	if(e.getKeyCode() == KeyEvent.VK_LEFT) {
	    Game.isLeftPressed = true;
	}
	if(e.getKeyCode() == KeyEvent.VK_RIGHT) {
	    Game.isRightPressed = true;
	}
	
	/**
	 * Event Keys
	 */
	if(e.getKeyCode() == KeyEvent.VK_A) {
	    // A = Interact
	    Game.isInteractPressed = true;
	}
	if(e.getKeyCode() == KeyEvent.VK_S) {
	    // S = Cancel
	    Game.isCancelPressed = true;
	    
	}
	if(e.getKeyCode() == KeyEvent.VK_D) {
	    // D = Menu
	    Game.isMenuPressed = true;
	}
	if(Game.debug == true) {
	    if(e.getKeyCode() == KeyEvent.VK_ESCAPE) {
		System.out.println("DEBUG EXIT");
		System.exit(0);
	    }   
	}
    }
    
    public void keyReleased(KeyEvent e) {
	if(e.getKeyCode() == KeyEvent.VK_UP) {
	    Game.isUpPressed = false;
	}
	if(e.getKeyCode() == KeyEvent.VK_DOWN) {
	    Game.isDownPressed = false;
	}
	if(e.getKeyCode() == KeyEvent.VK_LEFT) {
	    Game.isLeftPressed = false;
	}
	if(e.getKeyCode() == KeyEvent.VK_RIGHT) {
	    Game.isRightPressed = false;
	}
	
	/**
	 * Event Keys
	 */
	if(e.getKeyCode() == KeyEvent.VK_A) {
	    // A = Interact
	    Game.isInteractPressed = false;
	}
	if(e.getKeyCode() == KeyEvent.VK_S) {
	    // S = Cancel
	    Game.isCancelPressed = false;
	}
	if(e.getKeyCode() == KeyEvent.VK_D) {
	    // D = Menu
	    Game.isMenuPressed = false;
	}
    }
}
